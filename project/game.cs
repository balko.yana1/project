﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace project
{
    public partial class game : Form
    {
        bool left, right, shooting, isGameOver;
        int score1;
        int playerSpeed=12;
        int butterflySpeed;
        int flowerSpeed;
        Random rand = new Random();

        public game()
        {
            InitializeComponent();
            resetGame();
        }

        private void first_Click(object sender, EventArgs e)
        {

        }

        private void mainGameTimerEvent(object sender, EventArgs e)
        {
            score.Text = score1.ToString();
            first.Top += butterflySpeed;
            second.Top += butterflySpeed;
            third.Top += butterflySpeed;

            if (first.Top > 808|| second.Top > 808|| third.Top > 808)
            {
                gameOver();
            }



            //
            if (left == true && frog.Left>0)
            {
                frog.Left -= playerSpeed;
            }
            if (right == true && frog.Left < 639)
            {
                frog.Left += playerSpeed;
            }

            if (shooting == true)
            {
                flowerSpeed = 20;
                flower.Top -= flowerSpeed;
            }
            else
            {
                flower.Left = -300;
                flowerSpeed = 0; 
            }
            if(flower.Top<-30)
            {
                shooting = false;
            }
            if (flower.Bounds.IntersectsWith(first.Bounds))
            {
                score1 += 1;
                first.Top = -450;
                first.Left = rand.Next(0, 400);
                shooting = false;

            }
            if (flower.Bounds.IntersectsWith(second.Bounds))
            {
                score1 += 1;
                second.Top = -650;
                second.Left = rand.Next(0, 400);
                shooting = false;

            }
            if (flower.Bounds.IntersectsWith(third.Bounds))
            {
                score1 += 1;
                third.Top = -750;
                third.Left = rand.Next(0, 400);
                shooting = false;

            }
            if(score1 == 10)
            {
                butterflySpeed = 10;
            }

        }

        private void keyisdown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left)
            {
                left = true;
            }
            if (e.KeyCode == Keys.Right)
            {
                right = true;
            }
        }

        private void rock_Click(object sender, EventArgs e)
        {
           
        }

        private void game_Load(object sender, EventArgs e)
        {

        }

        private void keyisup(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Left)
            {
                left = false;
            }
            if (e.KeyCode == Keys.Right)
            {
                right = false;
            }
            if(e.KeyCode==Keys.Space&& shooting == false)
            {
                shooting = true;
                flower.Top = frog.Top-50;
                flower.Left = frog.Left + (frog.Width / 2);
            }
            if(e.KeyCode == Keys.Enter&& isGameOver == true)
            {
                resetGame();
            }
        }
        private void resetGame()
        {
            timer.Start();
            butterflySpeed = 6;

            first.Left = rand.Next(0, 400);
            second.Left = rand.Next(0, 400);
            third.Left = rand.Next(0, 400);

            first.Top = rand.Next(0, 200) * -1;
            second.Top = rand.Next(0, 500) * -1;
            third.Top = rand.Next(0, 900) * -1;

            score1 = 0;
            flowerSpeed = 0;
            flowerSpeed = -300;
            shooting = false;

            score.Text = score1.ToString();

        }
        private void gameOver()
        {
            isGameOver = true;
            timer.Stop();
            score.Text += Environment.NewLine + "Game over!!" + Environment.NewLine + "press enter to start.";

        }
    } 
}
