﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace project
{
    internal class File
    {
        

        public static void CreateTextFile(string fileName, DataGridView grid)
        {
            StreamWriter sw = new StreamWriter(fileName);
            for (int i = 0; i < grid.ColumnCount; i++)
            {
                sw.WriteLine(grid[i, 0].Value);
            }
            sw.Close();
        }

        public static void CreateDataFile(string fileName, DataGridView grid)
        {
            FileStream fs = new FileStream(fileName, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs);
            for (int i = 0; i < grid.ColumnCount; i++)
            {
                bw.Write(Convert.ToInt32(grid[i, 0].Value));
            }
            bw.Close();
            fs.Close();
        }



    }
}