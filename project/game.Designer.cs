﻿namespace project
{
    partial class game
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.score = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.flower = new System.Windows.Forms.PictureBox();
            this.frog = new System.Windows.Forms.PictureBox();
            this.second = new System.Windows.Forms.PictureBox();
            this.third = new System.Windows.Forms.PictureBox();
            this.first = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.flower)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.frog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.second)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.third)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.first)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // score
            // 
            this.score.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.score.Location = new System.Drawing.Point(-2, 416);
            this.score.Name = "score";
            this.score.Size = new System.Drawing.Size(688, 125);
            this.score.TabIndex = 5;
            this.score.Text = "0";
            this.score.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // timer
            // 
            this.timer.Interval = 20;
            this.timer.Tick += new System.EventHandler(this.mainGameTimerEvent);
            // 
            // flower
            // 
            this.flower.BackColor = System.Drawing.Color.White;
            this.flower.Image = global::project.Properties.Resources._45;
            this.flower.Location = new System.Drawing.Point(264, 563);
            this.flower.Name = "flower";
            this.flower.Size = new System.Drawing.Size(100, 50);
            this.flower.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.flower.TabIndex = 4;
            this.flower.TabStop = false;
            this.flower.Click += new System.EventHandler(this.rock_Click);
            // 
            // frog
            // 
            this.frog.BackColor = System.Drawing.Color.Transparent;
            this.frog.Image = global::project.Properties.Resources.frog;
            this.frog.Location = new System.Drawing.Point(523, 637);
            this.frog.Name = "frog";
            this.frog.Size = new System.Drawing.Size(163, 112);
            this.frog.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.frog.TabIndex = 3;
            this.frog.TabStop = false;
            // 
            // second
            // 
            this.second.Image = global::project.Properties.Resources.download;
            this.second.Location = new System.Drawing.Point(228, 243);
            this.second.Name = "second";
            this.second.Size = new System.Drawing.Size(116, 104);
            this.second.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.second.TabIndex = 2;
            this.second.TabStop = false;
            // 
            // third
            // 
            this.third.Image = global::project.Properties.Resources._24;
            this.third.Location = new System.Drawing.Point(12, 58);
            this.third.Name = "third";
            this.third.Size = new System.Drawing.Size(135, 103);
            this.third.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.third.TabIndex = 1;
            this.third.TabStop = false;
            // 
            // first
            // 
            this.first.Image = global::project.Properties.Resources._61;
            this.first.Location = new System.Drawing.Point(446, 97);
            this.first.Name = "first";
            this.first.Size = new System.Drawing.Size(130, 95);
            this.first.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.first.TabIndex = 0;
            this.first.TabStop = false;
            this.first.Click += new System.EventHandler(this.first_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::project.Properties.Resources.pic11;
            this.pictureBox1.Location = new System.Drawing.Point(-116, 455);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(802, 311);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::project.Properties.Resources.хмари;
            this.pictureBox3.Location = new System.Drawing.Point(446, 32);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(304, 191);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 8;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(19, 455);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(108, 75);
            this.pictureBox2.TabIndex = 7;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::project.Properties.Resources.хмари;
            this.pictureBox4.Location = new System.Drawing.Point(4, 119);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(304, 162);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 9;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::project.Properties.Resources.хмари;
            this.pictureBox5.Location = new System.Drawing.Point(283, 258);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(304, 191);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 10;
            this.pictureBox5.TabStop = false;
            // 
            // game
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(683, 761);
            this.Controls.Add(this.flower);
            this.Controls.Add(this.frog);
            this.Controls.Add(this.second);
            this.Controls.Add(this.third);
            this.Controls.Add(this.first);
            this.Controls.Add(this.score);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox5);
            this.Name = "game";
            this.Text = "game";
            this.Load += new System.EventHandler(this.game_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.keyisdown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.keyisup);
            ((System.ComponentModel.ISupportInitialize)(this.flower)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.frog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.second)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.third)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.first)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox first;
        private System.Windows.Forms.PictureBox third;
        private System.Windows.Forms.PictureBox second;
        private System.Windows.Forms.PictureBox frog;
        private System.Windows.Forms.PictureBox flower;
        private System.Windows.Forms.Label score;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
    }
}