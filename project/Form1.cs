﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace project
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void реєстраціяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Registration f2 = new Registration();
            f2.Show();
        }

        private void вийтиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void граToolStripMenuItem_Click(object sender, EventArgs e)
        {
            game f3 = new game();
            f3.Show();
        }
    }
}
