﻿using Microsoft.Office.Interop.Excel;
using project;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
namespace project
{
    internal class Excel1
    {
        public static void saveToExcel (DataGridView grid , string fileName, string sheetName)
        {
            Excel.Application excelApp  = new Microsoft.Office.Interop.Excel.Application();
            excelApp.Visible = true;
            Excel.Workbook person = excelApp.Workbooks.Add(Type.Missing);
            excelApp.SheetsInNewWorkbook = 1;
            excelApp.DisplayAlerts = false;
            Excel.Worksheet sheet = (Excel.Worksheet)excelApp.Worksheets.get_Item(1);
            sheet.Name = sheetName;
            int row = 1;
            
            for(int j=0; j < grid.ColumnCount; j++)
            {
                sheet.Cells[row, j + 1] = grid.Columns[j].HeaderText;
            }
            for(int i = 0; i < grid.RowCount; i++)
            {
                for(int j = 0; j < grid.ColumnCount; j++)
                {
                    sheet.Cells[row + i + 1, j + 1] = grid[j, i].Value;
                }
            }
            Excel.Range range = sheet.Range[sheet.Cells[row, 1], sheet.Cells[row, grid.ColumnCount]];
            range.Cells.Font.Name = "Arial";
            range.Cells.Font.Size = 12;
            range.Cells.Font.Color = ColorTranslator.ToOle(Color.Red);
            range.Interior.Color = ColorTranslator.ToOle(Color.Black);
            excelApp.Application.ActiveWorkbook.SaveAs($"{fileName}.xlsx");
            person.Close();
            excelApp.Quit();


        }
    }
}
